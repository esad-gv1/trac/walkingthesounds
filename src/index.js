import * as Mobilizing from '@mobilizing/library';
import { Script } from "./script.js";
import { Script2 } from "./scriptCapture.js";

// We need to run the script in a Mobilizing Context
// Build a simple function to instantiate the Context,
// instantiate the user script,
// add it as a component to the Context,
// and build a runner with it.

function checkPoint(){
    const context = new Mobilizing.Context();
    const script = new Script2();
    context.addComponent(script);
    const runner = new Mobilizing.Runner( {context} );

    //var divRun2 = document.createElement("div");
    //divRun2.style.display="block";
    //divRun2.style.position="absolute";
    //divRun2.style.float="right";
    //divRun2.style.right="0px";
    //divRun2.style.backgroundColor="green";
    //divRun2.style.height="80px";
    //divRun2.style.width="80px";
    //document.body.appendChild(divRun2);

    var check = false;
    setTimeout(() => {
        switchCheck();
    },6000);
    function switchCheck(){
        check = true;
        run();
    }
}
checkPoint();


function run() {
    const context = new Mobilizing.Context();
    const script = new Script();
    context.addComponent(script);
    const runner = new Mobilizing.Runner( {context} );
}


// Then run it

//run();