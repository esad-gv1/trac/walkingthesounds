import * as Mobilizing from '@mobilizing/library';
import Motion from '@mobilizing/library/dist/Mobilizing/input/Motion';
import Light from '@mobilizing/library/dist/Mobilizing/renderer/3D/three/scene/Light';
import Color from '@mobilizing/library/dist/Mobilizing/renderer/3D/three/types/Color';
import { TraiteurAcceleration } from "./tds";
import { Script2 } from "./scriptCapture.js";

export class Script {

    constructor() {

        this.accelProcessor = new TraiteurAcceleration(128 * 5, 0.01);

        console.log(this.accelProcessor);

        this.debugDiv = document.createElement("div");
        this.debugDiv.style.position = "fixed";
        this.debugDiv.style.color = "white";
        this.debugDiv.style.width = "300px";
        this.debugDiv.style.height = "200px";
        this.debugDiv.style.fontSize = "21px";
        //this.debugDiv.style.backgroundColor = "black";
        this.debugDiv.style.top = 0;
        this.debugDiv.style.left = 0;

        this.soundFilesNames = [
            "Son_1.1-rythme.wav",
            "Son_2.2-Bosses-11s.wav",
            "Son_2.4-Bosses-13s.wav",
            "Son_2.1-Bosses-13s.wav",
            "Son_2.3-Bosses-13s.wav",
            "rythmique_16_s.wav",
            "Son_5-2_flutepercuelectro.wav"

        ];
    }

    // called just after the constructor
    preLoad(loader) {

        this.soundFilesRequests = [];

        for (let i = 0; i < this.soundFilesNames.length; i++) {
            this.soundFilesRequests.push(loader.loadArrayBuffer({ "url": "assets/audio/" + this.soundFilesNames[i] }));
        }
    }

    // called when preloaded elements are loaded
    setup() {
        document.body.appendChild(this.captureDiv);
        document.body.appendChild(this.debugDiv);
        console.log(this.captureDiv);

        console.log("new captation : "+newCaptation);
        this.renderer = new Mobilizing.three.RendererThree({
            "preserveDrawingBuffer": true,
            "powerPreference": "high-performance",
            "shadowMapEnabled" : true
        });
        //this.renderer.shadowMapEnabled = true;
        this.context.addComponent(this.renderer);

        this.camera = new Mobilizing.three.Camera();
        this.camera.setAutoClearColor(false);
        this.renderer.addCamera(this.camera);

        //gyro cam control
        this.orientation = new Mobilizing.input.Orientation();
        this.context.addComponent(this.orientation);
        this.orientation.setup();

        this.gyroQuat = new Mobilizing.three.Quaternion();
        console.log(this.gyroQuat);

        const light = new Mobilizing.three.Light();
        light.castShadow = true;
        light.shadowDarkness = 1;
        light.shadowCameraVisible = true;
        console.log(light);
        light.transform.setLocalPosition(0,200,0);
        this.renderer.addToCurrentScene(light);


        //spot rouge qui tourne
        const light2 = new Mobilizing.three.Light({
            type : "point",
            color : Color.red
        });
        this.renderer.addToCurrentScene(light2);
        light2.transform.setLocalPosition(0,1,-100);
        //light2.transform.setLocalRotation(0,0,10000);
        light2.setTargetPosition(100,0,100);
        this.evolutionCirc = 0;
        this.parentLight2 = new Mobilizing.three.Node();
        this.renderer.addToCurrentScene(this.parentLight2);

        this.parentLight2.transform.addChild(light2.transform);

        //!!!!!! Déclaration de ACCEL avec mobilizing !!!!!!!!!

        this.accel = new Mobilizing.input.Motion();
        this.accel = this.context.addComponent(this.accel);
        this.accel.setup();//set it up

        this.accel.events.on("acceleration", this.accelEvent.bind(this));

        //this is a simple plane renderer in front of everything
        //with transparency. Rendering accumulation erases slowly the scene
        //producing a kind of trail effect
        this.fadingScreen = new Mobilizing.three.Cube({
            "width": 20,
            "height": 20,
            "material": "basic"
        });
        this.fadingScreen.material.setTransparent(true);
        this.fadingScreen.material.setColor(Mobilizing.three.Color.black);
        this.fadingScreen.material.setOpacity(.09);

        this.renderer.addToCurrentScene(this.fadingScreen);
        this.fadingScreen.transform.setRenderOrder(-1);

        //to understand where we're looking at
        const debbugGyroCube = new Mobilizing.three.Cube({
            "material": "basic",
            "segments": 8
        });
        debbugGyroCube.transform.setLocalPosition(0, 0, 0);
        debbugGyroCube.transform.setLocalScale(1000);
        debbugGyroCube.material.setWireframe(true);
        const c = .3;
        const color = new Mobilizing.three.Color(c, c, c);
        debbugGyroCube.material.setColor(color);
        this.renderer.addToCurrentScene(debbugGyroCube);

        //adding the plane
        const floor = new Mobilizing.three.Plane({
            "width": 2050,
            "height": 2050,
            "widthSegments": 8,
            "heightSegments": 8,
        });
        floor.receiveShadow = true;

        const floorColor = new Mobilizing.three.Color();
        floorColor.setStyle("white");

        floor.material.setColor(floorColor);

        floor.transform.setLocalRotation(90, 0, 0);
        floor.transform.setLocalPosition(0, -10, 0);

        //this.renderer.addToCurrentScene(floor);

        //acc debbuging with shapes
        const debbugAccBallParent = new Mobilizing.three.Node();
        this.renderer.addToCurrentScene(debbugAccBallParent);

        debbugAccBallParent.transform.setLocalPositionZ(-20);

        const debbugAccBall = new Mobilizing.three.Sphere({
            segments:20
        });
        debbugAccBall.castShadow = true;
        debbugAccBall.receiveShadow = false;
        debbugAccBall.transform.setLocalPositionZ(-20);

        //structure parent-enfant de base
        debbugAccBallParent.transform.addChild(debbugAccBall.transform);

        //test blocage par loop
        var indexBlock=0;

        Isblocked();
        function Isblocked(){
            if(indexBlock<15000){
                console.log(indexBlock);
                add1Block();
            }
        }

        function add1Block(){
            setTimeout(function(){
                indexBlock++;
                Isblocked();
            },1000);
        }


        setInterval(() => {
            this.debugDiv.innerHTML = this.accelProcessor.getAccelerationMoyenne("x").toFixed(2) + "<br>";
            this.debugDiv.innerHTML += this.accelProcessor.getAccelerationMoyenne("y").toFixed(2) + "<br>";
            this.debugDiv.innerHTML += this.accelProcessor.getAccelerationMoyenne("z").toFixed(2);

            //manipule les valeurs venant de la classe de Valentin
            const meanAcc = new Mobilizing.three.Vector3(
                this.accelProcessor.getAccelerationMoyenne("x"),
                this.accelProcessor.getAccelerationMoyenne("y"),
                this.accelProcessor.getAccelerationMoyenne("z")
            );
            meanAcc.multiplyScalar(3);
            //applique sur la sphere de debbug!!
            debbugAccBall.transform.setLocalPosition(meanAcc);
        }, 100);

        //audio
        this.audioRenderer = new Mobilizing.audio.Renderer();
        this.context.addComponent(this.audioRenderer);

        //attach audio listener to 3D camera
        this.audioRenderer.setListenerTransform(this.camera.transform);

        this.soundSources = [];
        this.soundModels = [];

        //Contruction de l'objets mouvant complex 1
        this.objComplex1 = new Mobilizing.three.Sphere({
            "radius":2,
            "segments":20,
        });
        this.objComplex1.geometry.parameters.widthSegments = 6;
        this.objComplex1.geometry.parameters.heightSegments = 20;
        this.objComplex1.transform.setLocalPosition(5,0,-10);

        //console.log(this.objComplex1);

        //Objets physiques traitement individuel
        this.objA = new Mobilizing.three.Sphere();
        this.objA.radius = 2;
        this.objA.transform.setLocalPosition(50,0,10);
        //mouvement objA
        this.movA_X = 0; this.movA_Y = 0; this.movA_Z;
        this.renderer.addToCurrentScene(this.objA);


        //Objets physiques traitement individuel
        this.objB = new Mobilizing.three.Sphere();
        this.objB.radius = 2;
        this.objB.transform.setLocalPosition(-20,0,-10);
        //mouvement objA
        this.movB_X = 0; this.movB_Y = 0; this.movB_Z;
        this.renderer.addToCurrentScene(this.objB);


        //array des objets physiques
        this.objectGeo = [];

        for (let i = 0; i < this.soundFilesNames.length; i++) {

            const source = new Mobilizing.audio.Source({ "renderer": this.audioRenderer });
            this.context.addComponent(source);

            if (i !== 0) {
                source.set3D(true);
            }
            source.setLoop(true);
            source.setRefDistance(100);

            let sound = new Mobilizing.audio.Buffer({
                "renderer": this.audioRenderer,
                "arrayBuffer": this.soundFilesRequests[i].getValue(),
                "decodedCallback": () => {
                    source.setBuffer(sound);
                }
            });

            if (i !== 0) {
                this.objectGeo[i] = new Mobilizing.three.Octahedron({
                    "size": i * 3,
                })

                this.objectGeo[i].transform.setLocalPositionZ(-100);
                this.renderer.addToCurrentScene(this.objectGeo[i]);

                source.setTransform(this.objectGeo[i].transform);
            }

            //console.log(this.objectGeo);
            this.soundSources.push(source);

        }

        //console.log(this.soundSources);
        this.soundSources[5].setTransform(this.objA.transform);
        this.soundSources[6].setTransform(this.objB.transform);

        window.addEventListener("touchend", () => {
            this.activateSensors();
            this.playAllSounds();
        });

        window.addEventListener("click", () => {
            this.activateSensors();
            this.playAllSounds();
        });
    }

    activateSensors() {
        this.accel.on();//active it
        this.orientation.on();
    }

    playAllSounds() {
        for (let i = 0; i < this.soundSources.length; i++) {
            if (!this.soundSources[i].playing){
                if(i>=5){
                    this.soundSources[i].play();
                }
            }
        }
    }

    accelEvent(acc) {
        this.accelProcessor.ajouterEchantillon(acc.x, acc.y, acc.z);
    }

    // refresh loop
    update() {
        this.parentLight2.transform.setLocalPosition(100+Math.cos(this.evolutionCirc)*200,1,-100+Math.sin(this.evolutionCirc)*200);
        //this.evolutionCirc +=0.01;
        this.gyroQuat.setFromGyro(this.orientation.compass);

        if (this.gyroQuat) {
            this.camera.transform.setLocalQuaternion(this.gyroQuat);
        }

        for (let i = 0; i < this.soundModels.length; i++) {
            this.soundModels[i].transform.lookAt(this.camera.transform.getLocalPosition());
        }
        for (let i = 1; i < this.objectGeo.length; i++) {
            this.objectGeo[i].transform.lookAt(this.camera.transform.getLocalPosition());
        }

        let factor = 4000;
        //3D objects moves
        this.objectGeo[1].transform.setLocalPosition(100 * Math.cos(performance.now() / factor), 50 * Math.sin(performance.now() / factor), 0);

        factor = 4000;
        this.objectGeo[2].transform.setLocalPosition(100 * Math.cos(performance.now() / factor), 50, 100 * Math.sin(performance.now() / factor));

        factor = 4500;
        this.objectGeo[3].transform.setLocalPosition(100 * Math.cos(performance.now() / factor), 100 * Math.sin(performance.now() / factor), 100 * Math.sin(performance.now() / factor));

        factor = 4500;
        this.objectGeo[4].transform.setLocalPosition(200 * Math.cos(performance.now() / factor), 50 * Math.sin(performance.now() / factor), 50 * Math.sin(performance.now() / factor));
    }
}
